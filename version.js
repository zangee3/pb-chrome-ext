const fs = require('fs');
const manifest = require('./app/manifest.json');
const version = manifest.version.split('.');
const minor = 1 + +version.pop();
version.push(minor);
manifest.version = version.join('.');

fs.writeFileSync('./app/manifest.json', JSON.stringify(manifest, null, 2));


