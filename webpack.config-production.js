const path = require('path');
const webpack = require('webpack');
module.exports = {
  context: path.join(__dirname, 'src'),
  devtool: null,
  entry: {
    popup: './popup.js',
    background: './background.js',
    test: '../tests/index.js',
    'test-runner': './test-runner/index.js',
  },
  output: {
    path: __dirname + '/app',
    filename: '[name].js'
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
      },
      comments: false,
      mangle: false
    }),
  ],
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loaders: ['babel-loader'],
        // query: {
        //   presets: ['env', 'react'],
        //   plugins: ['transform-runtime']
        // }
      }, {
        test: /\.(sass|scss)$/,
        loaders: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.css$/,
        loaders: ['style-loader', 'css-loader']
      },
      {
        test: /\.(woff2?|eot|ttf|svg)$/,
        loaders: ['url-loader']
      }
    ]
  }
};
