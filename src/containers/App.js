import React from 'react';
import {connect} from 'react-redux';
import Spinner from '../components/Spinner';
import {query, group} from '../actions/db';
import Box        from 'grommet/components/Box';
import List        from 'grommet/components/List';
import ListItem        from 'grommet/components/ListItem';
import Meter        from 'grommet/components/Meter';
import Distribution        from 'grommet/components/Distribution';
import Tabs        from 'grommet/components/Tabs';
import Tab        from 'grommet/components/Tab';
import Button        from 'grommet/components/Button';
import RefreshIcon from 'grommet/components/icons/base/Refresh';
import {parseTime, createTree} from '../utils';
import Tree from '../components/Tree';

const App = props => {
  const {query, timer} = props;
  const timerTree = createTree(timer);
  console.warn(timerTree);

  const urls = Object.keys(timer).map(url => {
    const o = timer[url];
    return o;
  });
  const forceSync = ()=> {
    chrome.runtime.sendMessage({method: 'forceSync'});
  };

  return (
    <Tabs responsive={false}>
      <Tab title='DEV'>
        <Button label='Force Sync' onClick={forceSync} />
        <Button label='Tests Runner' href='test-runner.html' target='_blank' />
      </Tab>
      <Tab title='Tree'>
        <Tree tree={timerTree} />
      </Tab>
      <Tab title='URLS list'>
        <List>
          {
            Object.keys(timer)
              .filter(a => !!a)
              .sort((b, a) => timer[a].duration - timer[b].duration)
              .map((url, i) => {
                const item = timer[url];
                const isApp = item.info.app;
                const icon = item.info && item.info.app && item.info.icons && item.info.icons[0].url;
                return <ListItem key={i} title={url}
                  className='urllist-listitem'>
                  <div>
                    {isApp ? <img height='32' src={icon} /> : null}
                    {isApp ? item.info.app.name : item.info.title || url}
                  </div>
                  <div>
                    {parseTime(item.duration / 1000)} / {parseTime(item.inactivetime / 1000)}
                  </div>
                </ListItem>
              })
          }
        </List>
      </Tab>
    </Tabs>
  );



  if(!timer.initialized) query();
  const series = Object.keys(timer).map(a => ({
    label: a + ' ' + parseTime(timer[a].duration / 1000),
    value: timer[a].duration,
    labelValue: a
  }));
  for(let k in timer) {
    timer[k].durationStr = parseTime(timer[k].duration / 1000);
  }
  return (
    <div>
      <pre>
        {JSON.stringify(Object.keys(timer)
          .sort((b, a) => timer[a].duration - timer[b].duration)
          .map(a => ({isApp: !!timer[a].info.app, url: a, duration: timer[a].durationStr})), null, 2) }
        </pre>

      </div>
  );
  return (
    <Tabs responsive={false}>
      <Tab title='Hosts'>
        <Box direction='row' justify='between' responsive={false}>
        </Box>
        <List>
          {
            Object.keys(timer)
              .sort((b, a) => timer[a].duration - timer[b].duration)
              .map((e, i) => {
                return <ListItem key={i} justify='start' responsive={false} primary={true} separator='right'>
                  <img className='favIcon' width='32' src={timer[e].favIcon} />
                  <Box size='large'>{e}</Box>
                  <Box pad='small'>{ parseTime(timer[e].duration / 1000)}</Box>
                  <Box direction='row'><Meter value={timer[e].duration} max={e.max} /></Box>
                </ListItem>
              })
          }
        </List>
      </Tab>
      <Tab title='Distribution'>
        <Distribution series={series} />
      </Tab>
    </Tabs>
  );
};

const mapStateToProps = state => ({
  timer: state.timer
});
const mapDispatchToProps = dispatch => {
  return {
    query: q => dispatch(group(q))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

