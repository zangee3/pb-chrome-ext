import React from 'react';
import {parseTime} from '../utils';
import {connect} from 'react-redux';
class Timer extends React.Component {
  constructor(props) {
    super(props);
    this.time = 0;
    window.Timer = {this, props};
  }

  componentWillMount() {
    const {timer} = this.props;
    this.time = timer.time;
    setTimeout(_ => this.redraw(), 100);
    if(timer.started) {
      this.__timer = setInterval(::this.redraw, 100);
    }
  }
  componentWillReceiveProps(nextProps) {
    this.componentWillUnmount();
    console.warn('componentWillReceiveProps()', nextProps);
    if(nextProps.timer.started) {
      this.time = nextProps.timer.time;
      this.__timer = setInterval(::this.redraw, 100);
    }
  }

  redraw() {
    this.refs.time.innerHTML = parseTime(this.time++);
  }

  componentWillUnmount() {
    clearInterval(this.__timer);
  }

  render() {
    return <div>
      <div ref='time' />
    </div>
  }
}

const mapStateToProps = state => {
  return {
    timer: state.timer
  };
};
export default connect(mapStateToProps)(Timer);
