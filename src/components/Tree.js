import React from 'react';
import DocumentIcon from 'grommet/components/icons/base/Domain';
import AppIcon from 'grommet/components/icons/base/Cli';
import UITree from 'react-ui-tree';
import Box        from 'grommet/components/Box';
export default class Tree extends React.Component {
  constructor(props) {
    super();
    this.state = {
      tree: props.tree
    };
  }
  toggleNode(node) {
    node.collapsed = !node.collapsed;
    this.setState(this.state);
  }
  renderNode (node) {
    const toggleNode = this.toggleNode.bind(this);
    return (
      <Box responsive={false} direction='row'
        justify='between' onClick={e => toggleNode(node)}>
        <div>{
          node.isApp ?
            <img src={node.icon} height='32' /> :
            <DocumentIcon style={{marginRight: 4, verticalAlign: 'middle'}}/>
        }
        <span title={node.url}>{node.name}</span>
      </div>
      <div>
        {node.durationStr ? node.durationStr : ''}
      </div>
    </Box>
    );
  }
  render() {
    return (
      <div>
        <UITree
          paddingLeft={20}
          tree={this.state.tree}
          onChange={e => {}}
          renderNode={this.renderNode.bind(this)}
        />
      </div>
    );
  }
}
