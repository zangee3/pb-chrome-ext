import React from 'react';

export default class Spinner extends React.Component {
  render() {
    const width = 32, height = width;
    const cx = width/2,
      cy = height/2,
      strokeColor = 'cadetblue',
      strokeWidth = 4,
      r = width * 0.4 - strokeWidth;
    return (<svg className='loading-icon' width={width} height={height}>
      <circle cx={cx} cy={cy} r={r} stroke={strokeColor} strokeWidth={strokeWidth} fill='none' />
      <circle cx={cx} cy={cy} r={r*0.9} stroke={strokeColor} strokeWidth={strokeWidth * 0.9} fill='none' />
      <circle cx={cx} cy={cy} r={r*0.7} stroke={strokeColor} strokeWidth={strokeWidth * 0.8} fill='none' />
      <circle cx={cx} cy={cy} r={r*0.5} stroke={strokeColor} strokeWidth={strokeWidth * 0.6} fill='none' />
      <circle cx={cx} cy={cy} r={r*0.3} stroke={strokeColor} strokeWidth={strokeWidth * 0.5} fill='none' />
    </svg>);
  }
}
