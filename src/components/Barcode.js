import React      from 'react';
import {connect}  from 'react-redux';
import jsBarcode    from 'jsbarcode';
import Box        from 'grommet/components/Box';
import {validationErrorAction, barcodeSuccessAction} from '../actions/barcode';

class Barcode extends React.Component{
  componentDidMount() {
    this.componentDidUpdate();
  }
  componentDidUpdate() {
    const {code, opts, validationError, barcodeSuccess} = this.props;
    if(!code) return;
    const {barcode} = this.refs;
    try {
    jsBarcode(barcode, code, this.props);
      barcodeSuccess(barcode.toDataURL());
    } catch(e) {
      validationError(e);
    }
  }
  render() {
    return (<Box textAlign='center' fill={false} size={'large'} direction='row' responsive={false}>
      <canvas ref="barcode" />
    </Box>);
  }
}
const mapDispatchToProps = dispatch => {
  return {
    validationError(err) {dispatch(validationErrorAction(err)); },
    barcodeSuccess(imgData) {dispatch(barcodeSuccessAction(imgData)); },
  };
};
export default connect(null, mapDispatchToProps)(Barcode);
