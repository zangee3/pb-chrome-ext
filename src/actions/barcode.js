export const BARCODE_UPDATE           = 'BARCODE_UPDATE';
export const BARCODE_UPDATE_TYPE      = 'BARCODE_UPDATE_TYPE';
export const BARCODE_FILTER_TYPES     = 'BARCODE_FILTER_TYPES';
export const BARCODE_VALIDATION_ERROR = 'BARCODE_VALIDATION_ERROR';
export const BARCODE_SUCCESS          = 'BARCODE_SUCCESS';
export const BARCODE_PRINT            = 'BARCODE_PRINT';

export const updateBarcodeAction = function updateBarcode(code) {
  return {
    type: BARCODE_UPDATE,
    payload: code
  };
}

export const updateBarcodeTypeAction = function updateBarcodeType(type) {
  return {
    type: BARCODE_UPDATE_TYPE,
    payload: type
  };
}

export const filterBarcodeTypesAction = function filterBarcodeTypes(query) {
  return {
    type: BARCODE_FILTER_TYPES,
    payload: query
  }
}

export const validationErrorAction = function validationError(err) {
  return {
    type: BARCODE_VALIDATION_ERROR,
    payload: err
  };
}

export const barcodeSuccessAction = function barcodeSuccess(payload) {
  return {type: BARCODE_SUCCESS, payload};
}

export const printBarcodeAction = function printBarcode(payload) {
  return {type: BARCODE_PRINT, payload};
}
