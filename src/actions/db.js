import DB, {add as addEntry} from '../db';
import {parseTime} from '../utils';

export const GET_TIME_LOG  = 'GET_TIME_LOG';
export const TIMER_TOGGLE  = 'TIMER_TOGGLE';
export const GET_TIME_LOG_FAIL  = 'GET_TIME_LOG_FAIL';
export const GET_TIME_LOG_SUCCESS  = 'GET_TIME_LOG_SUCCESS';

const transform = arr => arr.map((a, i) => ({
  durationStr: parseTime(Math.trunc(a.duration / 1000)),
  startDate: new Date(a.start),
  endDate: new Date(a.end),
  ...a
}));
export function group(qry, field='host') {
  return (dispatch, query) => {
    dispatch({type: GET_TIME_LOG});
    const entries = {};
    let total = 0;
    DB.log.each(a => {
      const {host, favIcon, duration} = a;
      const f = a[field];
      entries[f] = entries[f] || {
        ...a,
        duration: 0
      };
      total += duration / 1000;
      entries[f].duration += isFinite(duration) ? duration / 1000 : 0;
      entries[f].favIcon = entries[f].favIcon || a.favIcon;
    }).then(_ => {
      const payload = Object.keys(entries).map(k => entries[k]);
      payload.sort((a, b) => b.duration - a.duration).forEach(a => {
        a.max = total;
        a.ratio = Math.trunc(100 * a.duration / total);
        a.durationStr = parseTime(Math.trunc(a.duration))
      });

      dispatch({
        type: GET_TIME_LOG_SUCCESS,
        payload
      });
    });
  };
}
export function query(qry) {
  return (dispatch, getState) => {
    dispatch({type: GET_TIME_LOG});
    DB.log
      .where('duration')
      .above(1000)
      .toArray().then(res => {
        dispatch({
          type: GET_TIME_LOG_SUCCESS,
          payload: transform(res)
        });
      }).catch(err => {
        dispatch({
          type: GET_TIME_LOG_FAIL,
          error: err
        });
      });
  };
}
