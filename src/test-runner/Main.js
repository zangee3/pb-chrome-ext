import React, {Component} from 'react';
import ReactDOM           from 'react-dom';
import Box                from 'grommet/components/Box';
import Button             from 'grommet/components/Button';
import Table              from 'grommet/components/Table';
import TableHeader        from 'grommet/components/TableHeader';
import TableRow           from 'grommet/components/TableRow';
import OpenIcon           from 'grommet/components/icons/base/FolderOpen';
import CloseIcon          from 'grommet/components/icons/base/Close';
import FocusIcon          from 'grommet/components/icons/base/Select';
import CheckmarkIcon      from 'grommet/components/icons/base/Checkmark';
import InProgressIcon     from 'grommet/components/icons/base/Run';
import PauseIcon          from 'grommet/components/icons/base/Pause';
import {parse}            from 'papaparse';
import {
  createRunner,
  createTabFn,
  removeTabFn,
  focusTabFn
} from '../../tests/tabs-utils';

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data
    };
    this.runner = createRunner(1);
    this.runner.onRunListener(this.onRun.bind(this));
    this.runner.onFinish(this.onFinish.bind(this));
  }
  onFinish() {
    this.setState({...this.state, running: false});
  }
  onRun(i) {
    const data = this.state.data;
    var curr = data[this.currJobIndex];
    if(curr) curr.done = true;
    curr = data[i];
    if(curr) curr.started = true;
    this.setState({...this.state, data});
    this.currJobIndex = i;
  }
  getCallback(e) {
    switch(e.action) {
      case 'open':
        return createTabFn(e.url);
      case 'close':
        return removeTabFn(e.url);
      case 'focus':
        return focusTabFn(e.url);
      default:
        throw new Error('unsupported Action | ' + e.action);
    }
  }
  start() {
    this.setState({...this.state, running: true});
    for(let e of this.state.data) {
      const fn = this.getCallback(e);
      this.runner(fn, e.delay);
    }
  }
  stop() {
    this.currJobIndex = undefined;
    this.runner.stop();
    this.setState({...this.state, running: false});
  }
  loadCSV() {
    this.refs.file.click();
  }
  onFileLoad(e) {
    const file = e.target.files[0];
    parse(file, {
      header: true,
      skipEmptyLines: true,
      complete: (...args) => this.onCSVParse(...args),
      error(err) { throw err; }
    });
    e.target.value = '';
  }
  onCSVParse(csv) {
    csv.data = csv.data.map(a => ({
      ...a,
      delay: parseFloat(a.delay)
    }));
    this.props.save(csv.data);
    this.setState({...this.state, data: csv.data});
  }
  getIcon(action) {
    switch(action) {
      case 'open':
        return <OpenIcon />
      case 'close':
        return <CloseIcon />
      case 'focus':
        return <FocusIcon />
      default:
        return null;
    }
  }
  getTable() {
    return (<Table>
      <TableHeader labels={['', 'URL', 'Action', 'Delay']} />
      <tbody>
        {
          this.state.data.map((a, i) => (<TableRow key={i}>
            <td> {a.done ? <CheckmarkIcon /> : a.started ? <InProgressIcon /> : <PauseIcon /> } </td>
            <td> {a.url} </td>
            <td> {this.getIcon(a.action)} {a.action} </td>
            <td> {a.delay} </td>
          </TableRow>))
        }
      </tbody>
    </Table>);
  }
  render() {
    return (
      <div>
        <input type='file' style={{display: 'none'}} ref='file' onChange={e => this.onFileLoad(e)} accept='.csv' />
        <Box>
          <Box direction='row'>
            <Button onClick={e => this.loadCSV()} label='Load CSV'/>
            <Button onClick={e => this.state.running ? this.stop() : this.start()}
              label={this.state.running ? 'Stop' : 'Start'}/>
          </Box>
        </Box>
        <Box>
          {this.state.data ? this.getTable() : null}
        </Box>
      </div>
    );
  }
};


window.createRunner = createRunner;
