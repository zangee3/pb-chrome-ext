import {createStore, combineReducers} from 'redux';
import syncStorage from 'redux-localStorage';

const INIT_STATE = {
  data: [{
    url: 'http://0.0.0.0',
    action: 'open',
    delay: 1
  }]
};
const reducers = combineReducers({
  runner(state=INIT_STATE, {type, payload}) {
    switch(type) {
      case 'SAVE':
        state = {...state, data: payload};
        break;
    }
    return state;
  }
});
const store = createStore(reducers, syncStorage(['runner'], {key: 'runner'}));
export default store;
