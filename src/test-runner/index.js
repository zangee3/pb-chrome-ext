import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Provider, connect} from 'react-redux';
require('grommet/grommet-hpinc.min.css');
import GApp        from 'grommet/components/App';
import Main        from './Main';
import store from './store';


const baseUrl = 'https://dev.chrome-ext-backend.tk';
const serverUrl = `${baseUrl}/api/log/v2`;
const metricsUrl = `${baseUrl}/api/metrics`;
const START = Date.now();
const unix = () => Math.trunc(Date.now() / 1000);
var currTabId, backgroundCtx, globals;
const createdTabs = [];
import {
  createRunner,
  createTabFn,
  removeTabFn,
  focusTabFn
} from '../../tests/tabs-utils';

const run = createRunner(1);

function print(msg) {
  return function () {
    console.info(msg);
  }
}


const mapDispatchToProps = dispatch => ({
  save(payload) {
    dispatch({type: 'SAVE', payload});
  }
});
const mapStateToProps = state => ({
  data: state.runner.data
});
const MainC = connect(mapStateToProps, mapDispatchToProps)(Main);
const App = props => (
  <Provider store={store}>
    <GApp centered={true}>
      <MainC />
    </GApp>
  </Provider>
);

ReactDOM.render(<App />, document.getElementById('app'));
