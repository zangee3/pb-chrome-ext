import {groupBy} from 'lodash';
export function parseTime(seconds) {
  seconds = Math.trunc(seconds);
  var pad = function(x) { return (x < 10) ? "0"+x : x; }
  return pad(parseInt(seconds / (60*60))) + ":" +
    pad(parseInt(seconds / 60 % 60)) + ":" +
    pad(seconds % 60)
}

export function createTree(obj) {
  let children = Object.keys(obj)
    .filter(a => !!a && obj[a].duration > 1000)
    .sort((b, a) => obj[a].duration - obj[b].duration)
    .map(url => {
      const elm = obj[url];
      elm.url = url;
      elm.isApp = !!obj[url].info.app;
      if(elm.isApp) {
        elm.icon = obj[url].info.app.icon && sobj[url].info.app.icons[0].url;
      }
      elm._url = new URL(url);
      elm.domain = elm._url.origin.split('/').pop();
      return elm;
    });
  children = groupBy(children, 'domain');
  children = Object.keys(children)
    .map(a => {
      let o = {
        name: a,
        isApp: children[a][0].isApp,
        children: children[a].map(a => {
          a.name = a.info.title || decodeURIComponent(a._url.pathname + a._url.search);
          a.durationStr = parseTime(a.duration / 1000);
          return a;
        })
      };
      if(o.isApp) {
        o.children[0].name = o.children[0].info.app.name;
        return o.children[0];
      } else {
        const dur= o.children.reduce((x, y) => (x) + (y.duration || 0), 0);
        o.durationStr = parseTime(dur / 1000);
      }
      return o;
    });
  let tree = {
    name: '',
    children
  };
  return tree;
}
