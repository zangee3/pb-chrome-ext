import {EventEmitter} from 'events';
class IndexedDB extends EventEmitter{
  constructor(dbname='timelog', version=1) {
    super();
    this.version = version;
    this.dbname  = dbname;
    this.open();
  }
  open() {
    const req = window.indexedDB.open(this.dbname, this.version);
    req.onsuccess = e => {
      this.emit('open', e);
      this.db = e.target.result;
    }
    req.onerror = e => this.emit('open_error', e);
    req.onupgradeneeded = e => this.createStore(e.target.result)
  }
  createStore(db) {
    this.objStore = db.createObjectStore('log', {keypath: 'id', autoIncrement: true});
    this.objStore.createIndex('domain', 'domain', {unique: false});
    this.objStore.transaction.oncomplete = e => this.emit('ready', e);
  }
  add(row) {
    const transaction = this.db.transaction(['log'], 'readwrite').objectStore('log');
    transaction.add(row);
  }

}

const db = new IndexedDB();
db.on('open', console.warn.bind(console));
db.on('ready', console.info.bind(console));
export default db;
