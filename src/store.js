import {compose, createStore, applyMiddleware} from 'redux';
import createLogger                            from 'redux-logger';
import rootReducer                             from './reducers';
import thunk                                   from 'redux-thunk';
import persistState                            from 'redux-localStorage';

const logger = createLogger({
  collapsed: true
});
const store = createStore(
  rootReducer,
  applyMiddleware(logger, thunk),
);
window.store = store;
export default store;
