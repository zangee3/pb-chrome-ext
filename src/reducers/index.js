import {combineReducers} from 'redux';
import timer from './timer';
const dummy = (state={}, action) => state;
export default combineReducers({
  timer,
  dummy
});
