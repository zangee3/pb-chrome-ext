import {
  BARCODE_UPDATE,
  BARCODE_UPDATE_TYPE,
  BARCODE_FILTER_TYPES,
  BARCODE_VALIDATION_ERROR,
  BARCODE_SUCCESS,
  BARCODE_PRINT
} from '../actions/barcode';
const BARCODE_TYPES = {
  CODE128: [ 'CODE128', 'CODE128A', 'CODE128B', 'CODE128C' ],
  EAN: [ 'EAN2', 'EAN5', 'EAN8', 'EAN13', 'UPC' ],
  CODE39: ['CODE39'],
  ITF14: ['ITF14'],
  MSI: ['MSI', 'MSI10', 'MSI11', 'MSI1010', 'MSI1110'],
  pharmacode: ['pharmacode'],
  codabar: ['codabar']
};

const barcodeTypes = Object.keys(BARCODE_TYPES)
  .map(k => BARCODE_TYPES[k])
  .reduce((a, b) => [...a, ...b], []);
const TYPES = [...barcodeTypes];
const INIT_STATE = {
  code: '123456789012',
  format: 'EAN13',
  BARCODE_TYPES,
  barcodeTypes
};
function barcodeReducer(state=INIT_STATE, action) {
  switch(action.type) {
    case BARCODE_UPDATE:
      state = {...state, code: action.payload};
      break;
    case BARCODE_UPDATE_TYPE:
      state = {...state, format: action.payload};
      break;
    case BARCODE_FILTER_TYPES:
      state = {...state, barcodeTypes: TYPES.filter(a => a.toLowerCase().includes(action.payload))}
      break;
    case BARCODE_VALIDATION_ERROR:
      state = {...state, error: action.payload};
      break;
    case BARCODE_SUCCESS:
      state = {...state, error: null, image: action.payload};
      break;
    case BARCODE_PRINT:
      printBarcode(state, action.payload);
      break;
  }
  return state;
}
export default barcodeReducer;

function printBarcode(state, download) {
  const docDef = {
    pageSize: 'A6',
    pageOrientation: 'landscape',
    content: [
      {image: state.image}
    ]
  };
  if(download) {
    window.pdfMake.createPdf(docDef).download('barcode.pdf', a => {});
  } else {
    window.pdfMake.createPdf(docDef).print();
  }
}
