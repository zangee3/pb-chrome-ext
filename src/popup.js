import ReactDOM   from 'react-dom';
import React      from 'react';
import {Provider} from 'react-redux';
import store      from './store.js';
import Main       from './containers/App';
import App        from 'grommet/components/App';
import Box        from 'grommet/components/Box';
import Header     from 'grommet/components/Header';
import Title      from 'grommet/components/Title';
import Button      from 'grommet/components/Button';
import RefreshIcon from 'grommet/components/icons/base/Refresh';
import {group} from './actions/db';
require('grommet/grommet-hpinc.min.css');
require('./popup.sass');

class Popup extends React.Component {
  render() {
    let userId = 'Could not get user\'s email';
    try {
      userId = atob(localStorage.userId);
    } catch(e) { }
    return(
      <Provider store={store}>
        <App inline={false} centered={true}>
          <Header pad='small' colorIndex='neutral-1'>
            {/* <Title size='small' responsive={false}> */}
              Tabs Tracking (userId: {userId})
              {/* <Box size='small' /> */}
              {/* <Button secondary={true} title='Refresh' onClick={e => store.dispatch(group())} icon={<RefreshIcon />} /> */}
              {/* </Title> */}
          </Header>
          <Main />
        </App>
      </Provider>
    );
  }
}

ReactDOM.render(<Popup />, document.getElementById('app'));

// setInterval(location.reload.bind(location), 15000);
