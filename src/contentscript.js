function notify(e) {
  chrome.runtime.sendMessage({
    method: 'check',
    host: (location.host || location.hostname),
    event: e.type
  });
}


document.addEventListener('play',  notify, true);
document.addEventListener('pause', notify, true);
document.addEventListener('stop',  notify, true);
window.addEventListener('beforeunload',  e => notify({type: 'pause'}), true);
