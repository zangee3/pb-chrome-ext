import ChromePromise from 'chrome-promise';
import {debounce} from 'lodash';
import semver from 'semver';
const Chrome = new ChromePromise();
const TWELVE_HOURS = 12 * 60 * 60 * 1000,
  ONE_HOUR = 3600 * 1000,
  ONE_DAY = TWELVE_HOURS * 2,
  SIX_HOURS = TWELVE_HOURS / 2,
  FIVE_MINUTES = 5 * 60 * 1000,
  TEN_MINUTES = FIVE_MINUTES * 2;
const DISABLE_SERVER_SYNC = false;
var ignoreAppsWatch = false;
var platformInfo;
const DEBUG = false && process.env.NODE_ENV !== 'production';
function _log() {

}
let UPDATE_URL = 'https://postapi.paperbasket.com/prod/v1/api';
//UPDATE_URL = 'http://fdora:6006/api/log/v2';
function unix() {
  return Math.trunc(Date.now() / 1000);
}

function isValidUrl(url) {
  return (url.length <= 2048 && !url.startsWith('data'));
}
let _nextUpdate;

function debug(name, level='info', ...args) {
  const COLLAPSE = true;
  const _args = [
    '%c%s %c%s',
    `color: green;`,
    d.toLocaleDateString() + ' ' + d.toLocaleTimeString(),
    `color: red`,
    name
  ];
  if(COLLAPSE) console.groupCollapsed(_args);
  else console.group(_args);
  for(let arg of args) console[level](arg);
  console.groupEnd(name);
}

const urlsMap = localStorage.urlsMap ? JSON.parse(localStorage.urlsMap) : {};
function loadTabs() {
  chrome.windows.getAll({windowTypes: ['app', 'normal', 'popup', 'panel'], populate: true}, windows => {
    for(let win of windows) {
      for(let tab of win.tabs) {
        if(!isValidUrl(tab.url)) continue;
        if(urlsMap[tab.url]) {
          urlsMap[tab.url].tabsIds.add(tab.id);
        } else {
          urlsMap[tab.url] = {
            duration: 0,
            inactivetime: 0,
            tabsIds: new Set([tab.id]),
            start: -1,
            info: tab
          };
        }
        urlsMap[tab.url].lifetime = urlsMap[tab.url].lifetime || [];
      }
    }
    defaultListener();
  });
}

async function removeInstalledExtensions() {
  const installedExtensions = await Chrome.management.getAll();
  const manifest = chrome.runtime.getManifest();
  const extName = manifest.name;
  const dups = installedExtensions
    .filter(a => !a.isApp && a.enabled && (a.name === extName))
    .sort((a, b) => semver.rcompare(a.version, b.version))
  for(let i=1; i < dups.length; i++) {
    chrome.management.setEnabled(dups[i].id, false);
  }
  return dups.slice(1);
}
window.removeInstalledExtensions = removeInstalledExtensions;


(() => {
  for(let k in urlsMap) {
    urlsMap[k].tabsIds = new Set();
    urlsMap[k].lifetime = urlsMap[k].lifetime || [];
  }
  loadTabs();
  attachListeners();
  getPlatfromInfo();
  removeInstalledExtensions();
})();

let currWindowId;
let currUrl;

function getAppId(url) {
  return url.split('/')[2];
}

const saveToLocalStorage = debounce(() => {
  localStorage.urlsMap = JSON.stringify(urlsMap);
}, 333);

let lastCheckTime = Date.now();
function _updateCurr() {
  if(currUrl && currUrl.start > 0) {
    currUrl.duration += Math.min(savePoolInterval, Date.now() - lastCheckTime);
    lastCheckTime = Date.now();
  }
}
function _clearCurr() {
  if(currUrl && currUrl.start > 0) {
    currUrl.duration += Math.min(savePoolInterval, Date.now() - currUrl.start);
    currUrl.start = -1;
    const lastEntry = currUrl.lifetime[currUrl.lifetime.length - 1];
    lastEntry.end = unix();
    if(!(lastEntry.end - lastEntry.start)) currUrl.lifetime.pop();
  }
  for(let k in urlsMap) {
    if((currUrl && currUrl.info.url === k)
      || !urlsMap[k].tabsIds.size
      || urlsMap[k].removed) continue;
    const d = Date.now() - lastCheckTime;
    if(d < FIVE_MINUTES) {
      urlsMap[k].inactivetime += Math.min(savePoolInterval, Date.now() - lastCheckTime);
    }
  }
  currUrl = null;
  lastCheckTime = Date.now();
}
function _resetCurr(tab) {
  if(!tab) return;
  if(!isValidUrl(tab.url)) return;
  urlsMap[tab.url] = urlsMap[tab.url] || {
    duration: 0,
    start: Date.now(),
    inactivetime: 0,
    tabsIds: new Set(),
    lifetime: [],
    info: tab
  };
  urlsMap[tab.url].tabsIds.add(tab.id);
  urlsMap[tab.url].lifetime.push({
    start: unix()
  });
  urlsMap[tab.url].info = tab;
  currUrl = urlsMap[tab.url];
  currUrl.start = Date.now();
}
function defaultListener() {
  const tabs = chrome.tabs.query({
    active: true
  }, tabs => {
    const tab = tabs[0];
    if(tab && tab.active) {
      // && tab.windowId === currWindowId
      _clearCurr();
      _resetCurr(tab)
    }
    // localStorage.urlsMap = JSON.stringify(urlsMap);
    saveToLocalStorage();
  }); ;
}
// const defaultListener = defaultListener_;

function createListener(name, fn=defaultListener) {
  const COLLAPSE = true;
  const d = new Date();
  const _args = [
    '%c%s %c%s',
    `color: green;`,
    d.toLocaleDateString() + ' ' + d.toLocaleTimeString(),
    `color: red`,
    name
  ];
  return function(...args) {
    if(DEBUG) {
      COLLAPSE ? console.groupCollapsed.apply(console, _args) : console.group.apply(console, _args);
      for(let arg of args) console.info(arg);
      console.groupEnd(name);
    }
    fn ? fn.apply(null, args) : void 0;
  };
}
async function onWindowFocusChange(id) {
  if(ignoreAppsWatch) return;
  _clearCurr();
  currWindowId = id;
  if(id === -1) {
    const appsWindows = await Chrome.windows.getAll({
      windowTypes: ['app', 'panel', 'popup'],
      populate: true
    });
    const activeApp = appsWindows.find(a => a.focused);
    if(activeApp) {
      const appTab = activeApp.tabs[0];
      appTab.app = await Chrome.management.get(getAppId(appTab.url));
      delete appTab.app.tabs;
      _resetCurr(appTab);
      currWindowId = activeApp.id;
    }
    // localStorage.urlsMap = JSON.stringify(urlsMap);
    saveToLocalStorage();
  } else {
    defaultListener();
  }
}
function onTabRemove(id, info, tab) {
  const isInactive = !currUrl || !currUrl.tabsIds.has(id);
  for(let k in urlsMap) {
    if(isInactive && urlsMap[k].tabsIds.has(id)) {
      urlsMap[k].inactivetime += Math.min(savePoolInterval, Date.now() - lastCheckTime);
    }
    urlsMap[k].tabsIds.delete(id);
  }
  defaultListener();
}
function onTabUpdated(id, info, tab) {
  if(info.url) {
    for(let k in urlsMap) {
      if(urlsMap[k].tabsIds.has(id)) {
        urlsMap[k].tabsIds.delete(id);
        break;
      }
    }
  }
  defaultListener();
}
function attachListeners() {
  chrome.tabs.onActivated.addListener(createListener('onActivated'));
  chrome.windows.onFocusChanged.addListener(createListener('onWindowFocusChange', onWindowFocusChange));
  chrome.tabs.onRemoved.addListener(createListener('tabs-onRemoved', onTabRemove));
  chrome.tabs.onUpdated.addListener(createListener('tabs-onUpdated', onTabUpdated));
}


const DEFAULT_SAVE_INTERVAL = FIVE_MINUTES;
let savePoolInterval = DEFAULT_SAVE_INTERVAL;
let savePoolTimer = 0;
function savePoolFn() {
  _updateCurr();
  _resetCurr(currUrl && currUrl.info);
  saveToLocalStorage();
  if(Date.now() > _nextUpdate) {
    serverSync().then(() => {
      scheduleUpdate();
      saveToLocalStorage();
    })
  }

}
clearInterval(savePoolTimer);
savePoolTimer = setInterval(savePoolFn, savePoolInterval);

Object.assign(window, {
  info: console.info.bind(console),
  getGlobals: {
    getUrlsMap() {
      return urlsMap;
    },
    getInstalledExtension,
    update: defaultListener,
    runTests: () => chrome.tabs.create({url: '/test.html'}),
    currUrl: () => currUrl,
    setUpdateServerUrl: u => UPDATE_URL = u,
    getUpdateServerUrl: () => UPDATE_URL,
    setSavePoolInterval(t) {
      clearInterval(savePoolTimer);
      savePoolInterval = t;
      savePoolTimer = setInterval(savePoolFn, savePoolInterval);
    },
    setNextUpdate(t) {
      _nextUpdate = t;
    },
    ignoreAppsWatch() {
      ignoreAppsWatch = false;
    },
    getAppsInfo,
    saveToLocalStorage,
    serverSync,
    encode,
    clearStorage: () => {
      localStorage.clear();
      for(let k in urlsMap) delete urlsMap[k];
    },
    dump() {
      for(let k in urlsMap) {
        const key = urlsMap[k].info.app ? urlsMap[k].info.app.name : k;
        console.log(
`${key}:
  active: ${Math.round(urlsMap[k].duration / 1000)}, inactive: ${Math.round(urlsMap[k].inactivetime / 1000)},
`
        );
      }
    },
    getUserId
  }
});

async function getUserId() {
  const userInfo = await Chrome.identity.getProfileUserInfo();
  return userInfo.email;
  let req = await fetch('https://mail.google.com/mail/u/0/', {credentials: 'include', mode: 'cors'});
  let html = await req.text();
  try {
    let globals = JSON.parse(html.match(/GLOBALS=(.+?),\[/)[1] + ']');
    _log(globals);
    return globals[10];
  } catch(e) {
  }
}


if(localStorage.nextUpdate) {
  _nextUpdate = parseInt(localStorage.nextUpdate);
} else {
  scheduleUpdate();
}

function scheduleUpdate() {
  _nextUpdate = Date.now() + ONE_HOUR + (Math.random() * TEN_MINUTES);
  localStorage.nextUpdate = _nextUpdate;
}

const ENC_KEY = 'b50af6e76a30776f0f1cc425960937c7f9b34d75fb741109f2c70ae2dc5f3ee4';

async function encode(s) {
  const pwUtf8 = new TextEncoder().encode(s);
  const pwHash = await crypto.subtle.digest('SHA-1', pwUtf8);
  return Buffer.from(pwHash).toString('hex');
}

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  switch(request.method) {
    case 'forceSync':
      serverSync().then(sendResponse)
      return true;
  }
});

async function getAppsInfo() {
  let apps = await Chrome.management.getAll();
  apps = apps.filter(a => a.isApp);
  return apps.reduce((a, b) => Object.assign(a, {[b.id]: b}), {});
}

async function serverSync() {
  await defaultListener();
  const key = `__sync__-${unix()}`;
  localStorage[key] = JSON.stringify(urlsMap);
  for(let k in urlsMap) delete urlsMap[k];
  // saveToLocalStorage();
  loadTabs();
  const now = unix();
  const localStorageKeys = Array.from({length: localStorage.length}, (a, i) => localStorage.key(i))
    .filter(a => a.startsWith('__sync__'));
  const responses = [];
  for(let i=0; i < localStorageKeys.length; i++) {
    let sKey = localStorageKeys[i];
    if(sKey.startsWith('__sync__')) {
      const data = JSON.parse(localStorage.getItem(sKey)),
        ts = Number(sKey.split('-').pop());
      try {
        const resp = await doServerSync(data, ts);
        responses.push(resp);
        localStorage.removeItem(sKey);
        chrome.browserAction.setBadgeText({text: ''});
      } catch(e) {
        chrome.browserAction.setBadgeBackgroundColor({color: 'red'});
        chrome.browserAction.setBadgeText({text: 'E'});
        console.warn(e);
        return;
      }
    }
  }
  return responses;
}

async function getDeviceInfo() {
  const deviceInfoPromisify = (fn) => new Promise((res, reject) =>
    chrome.enterprise.deviceAttributes[fn](res));
  const fns = {
    directoryDeviceId      : 'getDirectoryDeviceId',
    deviceSerialNumber     : 'getDeviceSerialNumber',
    deviceAssetId          : 'getDeviceAssetId',
    deviceAnnotatedLocation: 'getDeviceAnnotatedLocation',
  };
  const deviceInfo = {};
  for(let k in fns) {
    try {
      deviceInfo[k] = await deviceInfoPromisify(fns[k]);
    } catch(e) {
    }
  }
  return deviceInfo;
}

async function getInstalledExtension() {
  return (await Chrome.management.getAll()).reduce((a,b) =>
    Object.assign(a, {[b.id]: b}), {});
}

function getSourceAlias(url, extsMap) {
  if(!url.startsWith('chrome')) return;
  const u = new URL(url);
  const hostname = u.host || u.hostname;
  if(hostname in extsMap) return extsMap[hostname].name;
}

async function doServerSync(urlsMap, timestamp) {
  console.log('doServerSync', urlsMap);
  const installedExtensions = await getInstalledExtension();
  if(!localStorage.userId) {
    const userId = await getUserId();
    if(userId) localStorage.userId = btoa(userId);
  }
  const deviceInfo = await getDeviceInfo();
  const body = {};
  body.deviceInfo = deviceInfo;
  body.machineId = deviceInfo.deviceSerialNumber;
  body.platformInfo = platformInfo;
  body.os = platformInfo.os;
  body.clientVersion = chrome.runtime.getManifest().version;
  body.userId = atob(localStorage.userId);
  body.timestamp = timestamp;
  body.macAddress = '';
  const data = [];
  for(let k in urlsMap) {
    let key = urlsMap[k].info.app ? urlsMap[k].info.app.name : k;
    let elm = urlsMap[k];
    const activeTime = Math.floor(elm.duration / 1000),
      inactiveTime   = Math.floor(elm.inactivetime / 1000);
    if(activeTime || inactiveTime) {
      data.push({
        source: key,
        alias: getSourceAlias(key, installedExtensions),
        activeTime,
        inactiveTime,
        lifetime: elm.lifetime,
        icon: elm.info.favIconUrl,
        type: elm.info.app ? 'app' : 'website'
      });
    }
  }
  body.data = data;
  if(DISABLE_SERVER_SYNC) return console.log(body);

  const req = await fetch(UPDATE_URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  });
  const resp = await req.json();

  lastCheckTime = Date.now();
  for(let k in urlsMap) {
    _log('deleting', k);
    delete urlsMap[k];
  }
  chrome.browserAction.setBadgeText({text: ''});
  return resp;
}

function getPlatfromInfo() {
  chrome.runtime.getPlatformInfo(info => platformInfo = info);
}

if(!localStorage.userId) {
  getUserId()
    .then(userId => localStorage.userId = btoa(userId))
}

