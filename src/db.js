import dexie from 'dexie';
const VERSION = 1;
const db = new dexie('log');
db.version(VERSION).stores({
  log: '++,timestamp,host,event,duration'
});
db.open().catch(e => {
  console.error('error openning DB', e);
});

export function add(obj) {
  return db.log.add(obj);
}


export default db;
