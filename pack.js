const manifest = require('./app/manifest.json');
const {kebabCase} = require('lodash');
const {exec} = require('child_process');

exec(`zip -9 ../${kebabCase(manifest.name)}-${manifest.version}.zip ./images/*.png ./*.js* ./*.html -x *.map`, {
  cwd: './app'
}, console.log);
