I need an app/extension for Chrome OS that tracks time spent on a particular
website(s). The idea is I want to track how much time they spent on
apps/websites using Chrome OS laptops and then send this data to my admin
portal. The app/extension need to track time for active tabs and inactive time
for tabs that are not active/in the foreground.  For example, the data sent to
  the admin server should look like the following:

UserID: xxx
URL1: {activetime: xx, inactivetime: xx}
URL2: {activetime: xx, inactivetime: xx}
URL2: {activetime: xx, inactivetime: xx}
APP1: {activetime: xx, inactivetime: xx}
APP2: {activetime: xx, inactivetime: xx}
APP3: {activetime: xx, inactivetime: xx}
