const path = require('path');
module.exports = {
  context: path.join(__dirname, 'src'),
  devtool: 'source-map',
  entry: {
    popup: './popup.js',
    background: './background.js',
    sandbox: './sandbox.js',
    contentscript: './contentscript.js',
    test: '../tests/index.js',
    'test-runner': './test-runner/index.js',
  },
  output: {
    path: __dirname + '/app',
    filename: '[name].js'
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel',
        query: {
          presets: ['react', 'env'],
          plugins: ['transform-runtime']
        }
      }, {
        test: /\.(sass|scss)$/,
        loaders: ['style', 'css', 'sass']
      },
      {
        test: /\.css$/,
        loaders: ['style', 'css']
      },
      {
        test: /\.(woff2?|eot|ttf|svg)$/,
        loaders: ['url']
      }
    ]
  }
};
