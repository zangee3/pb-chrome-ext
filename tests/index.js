const baseUrl = 'https://dev.chrome-ext-backend.tk';
const serverUrl = `${baseUrl}/api/log/v2`;
const metricsUrl = `${baseUrl}/api/metrics`;
window.jasmine.DEFAULT_TIMEOUT_INTERVAL = 20 * 1000;
const START = Date.now();
const unix = () => Math.trunc(Date.now() / 1000);
var currTabId, backgroundCtx, globals;
const createdTabs = [];
import {
  createRunner,
  createTabFn,
  removeTabFn,
  focusTabFn
} from './tabs-utils';

beforeAll((done) => {
  chrome.tabs.getCurrent(tab => currTabId = tab.id);
  chrome.runtime.getBackgroundPage(background => {
    expect(background).toBeTruthy();
    backgroundCtx = background;
    globals = backgroundCtx.getGlobals;
    window.bkg = background;
    done();
  });
});
const ONE_MINUTE = 60 * 1000,
  FIVE_MINUTES = ONE_MINUTE * 5,
  TEN_MINUTES = FIVE_MINUTES * 2,
  ONE_HOUR = ONE_MINUTE * 60,
  TWELVE_HOURS = 12 * ONE_HOUR,
  ONE_DAY = TWELVE_HOURS * 2;
xdescribe('init', () => {
  it('should set update URL to test server', () => {
    globals.setUpdateServerUrl(serverUrl);
    expect(globals.getUpdateServerUrl()).toEqual(serverUrl);
  });
  it('should manually sync data', (done) => {
    globals.serverSync().then(responses => {
      const success = responses.every(a => a.success);
      expect(success).toBeTruthy();
      done();
    });
  });
  it('should clear URLs map', (done) => {
    const urlsMap = globals.getUrlsMap();
    const cleared = Object.keys(urlsMap)
      .every(key => {
        const elm = urlsMap[key];
        return !(elm.duration > 1000 || elm.inactivetime > 1000)
          && (elm.tabsIds.size === 1);
      });
    expect(cleared).toBeTruthy();
    setTimeout(() => done(), 1000);
  });
  it('should automatically send data', (done) => {
    const tomorrow = Date.now() + (24 * 3600 * 1000);
    globals.setSavePoolInterval(8000);
    globals.setNextUpdate(Date.now());
    setTimeout(() => {
      globals.setSavePoolInterval(TEN_MINUTES);
      const tDiff = Number(localStorage.nextUpdate) - tomorrow;
      expect(tDiff).toBeLessThan(TEN_MINUTES);
      done();
    }, 10000);
  });
});

describe('Tabs timing', () => {
  it('should clear urlsMap', () => {
    const urlsMap = backgroundCtx.getGlobals.getUrlsMap();
    for(let k in urlsMap) delete urlsMap[k];
    expect(Object.keys(backgroundCtx.getGlobals.getUrlsMap()).length)
      .toEqual(0);
  });
  it('open a website and wait for 5 seconds (inactive)', (done) => {
    const url = 'https://www.gnu.org/doc/doc.html';
    chrome.tabs.create({url}, tab => {
      setTimeout(() => {
        // backgroundCtx.getGlobals.update();
        chrome.tabs.remove(tab.id);
      }, 6500);
      setTimeout(() => {
        backgroundCtx.getGlobals.update();
        chrome.tabs.update(currTabId, {active: true});
      }, 1000)
      setTimeout(() => {
        const urlsMap = backgroundCtx.getGlobals.getUrlsMap();
        expect(urlsMap[url]).toBeDefined();
        expect(urlsMap[url].inactivetime).toBeGreaterThan(4000);
        expect(urlsMap[url].duration).toBeLessThan(2000);
        done();
      }, 7000);
    });
  });
  it('open a website and wait for 5 seconds', (done) => {
    const url = 'https://www.gnu.org/';
    chrome.tabs.create({url}, tab => {
      setTimeout(() => {
        chrome.tabs.update(currTabId, {active: true});
        chrome.tabs.remove(tab.id);
      }, 7500);
      setTimeout(() => {
        const urlsMap = backgroundCtx.getGlobals.getUrlsMap();
        expect(urlsMap[url]).toBeDefined();
        expect(urlsMap[url].duration).toBeGreaterThan(5000);
        expect(urlsMap[url].inactivetime).toBeLessThan(10);
        done();
      }, 8000);
    });
  });
  it('open three identical tabs for 10 seconds and time them --inactivetime should be 0--', done => {
    const url = 'https://ftp.gnu.org/gnu/flex/';
    [1, 2, 3].forEach((i) => {
      chrome.tabs.create({url}, tab => {
        createdTabs.push(tab.id);
      });
    });
    setTimeout(() => {
      chrome.tabs.update(currTabId, {active: true});
      chrome.tabs.remove(createdTabs);
    }, 12700);
    setTimeout(() => {
      // const urlsMap = globals.getUrlsMap();
      const urlsMap = backgroundCtx.getGlobals.getUrlsMap();
      console.log(urlsMap[url]);
      expect(urlsMap[url]).toBeTruthy();
      expect(urlsMap[url].duration).toBeGreaterThan(10000);
      expect(urlsMap[url].inactivetime).toBeLessThan(300);
      done();
    }, 14200);
  });
});

const URLS_LIST = [
  'https://www.gnu.org/',
  'https://www.gnu.org/gnu/gnu.html',
  'https://www.gnu.org/philosophy/philosophy.html',
  'https://www.gnu.org/licenses/licenses.html',
  'https://www.gnu.org/software/software.html',
  'https://www.gnu.org/doc/doc.html',
];
describe('long test', () => {
  it('should clear urlsMap', () => {
    const urlsMap = backgroundCtx.getGlobals.getUrlsMap();
    for(let k in urlsMap) delete urlsMap[k];
    expect(Object.keys(backgroundCtx.getGlobals.getUrlsMap()).length)
      .toEqual(0);
  });
  it('should close other tabs', done => {
    chrome.tabs.query({}, tabs => {
      tabs = tabs.map(a => a.id)
        .filter(a => a !== currTabId);
      chrome.tabs.remove(tabs);
      done();
    });
  });
  it('should open tabs in sequence', (done) => {
    const run = createRunner();
    for(let i=0; i < URLS_LIST.length; i++) {
      run(createTabFn(URLS_LIST[i]), 5);
    }
    run(focusTabFn(0), 1);
    for(let i=0; i < URLS_LIST.length; i++) {
      run(focusTabFn(i + 1), 2);
    }
    for(let i=0; i < URLS_LIST.length; i++) {
      run(removeTabFn(URLS_LIST.length - i), 5);
    }
    run(function() {
      const urlsMap = backgroundCtx.getGlobals.getUrlsMap();
      for(let url of URLS_LIST) {
        const item = urlsMap[url];
        expect(item).toBeDefined();
        expect(item.tabsIds.size).toEqual(0);
      }
      for(let i=0; i < URLS_LIST.length - 1; i++) {
        expect(Math.round(urlsMap[URLS_LIST[i]].duration / 1000)).toBeGreaterThan(9.9);
      }
      expect(Math.round(urlsMap[URLS_LIST[URLS_LIST.length - 1]].duration / 1000)).toBeLessThan(8);
    }, 2);
    for(let url of URLS_LIST) run(createTabFn(url), 10);
    for(let i=0; i < URLS_LIST.length - 1; i++) run(removeTabFn(URLS_LIST.length - i), 10);
    run(function function_name() {
      const urlsMap = backgroundCtx.getGlobals.getUrlsMap();
      for(let i=0; i < URLS_LIST.length - 1; i++) {
        expect(Math.round(urlsMap[URLS_LIST[i]].duration / 1000)).toBeGreaterThan(19.9);
      }
      expect(Math.round(urlsMap[URLS_LIST[URLS_LIST.length - 1]].duration / 1000)).toBeLessThan(18);
    }, 2);
    for(let i=0; i < 10; i++) {
      run(createTabFn(URLS_LIST[0]), 30);
    }

    run(removeTabFn(1), 5);
    run(() => {
      const urlsMap = backgroundCtx.getGlobals.getUrlsMap();
      expect(Math.round(urlsMap[URLS_LIST[0]].duration / 1000)).toBeGreaterThan(18 + (10 * 29.0));
    }, 1);
    for(let i=0; i < 10; i++) run(removeTabFn(1), 1);
    run(() => done(), 1);
  }, ONE_HOUR);
});

afterAll(() => {

});
