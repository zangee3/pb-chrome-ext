export function createRunner(defaultDelay) {
  const queue = [];
  var delay = 0;
  var isRunning;
  var timer;
  var onRunListener = () => {};
  var onFinish = () => {};
  var onError = () => {};
  var i = 0;
  function run() {
    if(isRunning) return;
    isRunning = true;
    const next = queue.shift();
    onRunListener(i);
    if(!next) {
      isRunning = false;
      onFinish(i);
      i = 0;
      return;
    }
    i++;
    timer = setTimeout(() => {
      try {
      next[0]();
      } catch(e) {
        onError(e);
      }
      isRunning = false;
      run();
    }, next[1] * 1000);
  }
  function stop() {
    clearInterval(timer);
    for(let i=0; i < queue.length; i++) queue.pop();
  }
  function schedule(fn, _delay=defaultDelay) {
    queue.push([fn, _delay]);
    delay += _delay;
    run();
  }
  schedule.stop = stop;
  schedule.onRunListener = fn => onRunListener = fn;
  schedule.onFinish = fn => onFinish = fn;
  schedule.onError = fn => onError = fn;
  return schedule;
}

export function createTabFn(url, active=true) {
  return function() {
    chrome.tabs.create({url, active});
  }
}

export function removeTabFn(index) {
  var url;
  if(typeof(index) === 'string') {
    url = new URL(index).href;
    index = 0;
  }
  return function() {
    chrome.tabs.query({currentWindow: true, url}, tabs => {
      chrome.tabs.remove(tabs[index].id);
    });
  }
}

export function focusTabFn(index) {
  var url;
  if(typeof(index) === 'string') {
    url = new URL(index).href;
    index = 0;
  }
  return function() {
    chrome.tabs.query({currentWindow: true, url}, tabs => {
      chrome.tabs.update(tabs[index].id, {active: true});
    });
  }
}
